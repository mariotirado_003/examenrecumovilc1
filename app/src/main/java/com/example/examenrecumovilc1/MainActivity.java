package com.example.examenrecumovilc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private String nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editTextNombre = findViewById(R.id.editTextNombre);
        TextView textViewNombre = findViewById(R.id.textViewNombre);
        Button buttonImc = findViewById(R.id.button_imc);
        Button buttonTemp = findViewById(R.id.button_temp);
        Button buttonSalir = findViewById(R.id.button_salir);

        editTextNombre.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    nombre = editTextNombre.getText().toString();
                    textViewNombre.setText("Bienvenido, " + nombre);
                    return true;
                }
                return false;
            }
        });

        buttonImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ImcActivity.class);
                intent.putExtra("NOMBRE", nombre);
                startActivity(intent);
            }
        });

        buttonTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TempActivity.class);
                intent.putExtra("NOMBRE", nombre);
                startActivity(intent);
            }
        });

        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
