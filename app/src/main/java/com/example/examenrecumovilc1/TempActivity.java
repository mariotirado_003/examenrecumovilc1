package com.example.examenrecumovilc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class TempActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        TextView textViewNombreTemp = findViewById(R.id.textViewNombreTemp);
        EditText editTextTemp = findViewById(R.id.editTextTemp);
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        Button buttonConvertirTemp = findViewById(R.id.buttonConvertirTemp);
        TextView textViewResultadoTemp = findViewById(R.id.textViewResultadoTemp);
        Button buttonLimpiarTemp = findViewById(R.id.buttonLimpiarTemp);
        Button buttonRegresarTemp = findViewById(R.id.buttonRegresarTemp);

        String nombre = getIntent().getStringExtra("NOMBRE");
        textViewNombreTemp.setText("Bienvenido, " + nombre);

        buttonConvertirTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double temp = Double.parseDouble(editTextTemp.getText().toString());
                if (radioGroup.getCheckedRadioButtonId() == R.id.radioCtoF) {
                    double result = (temp * 9 / 5) + 32;
                    textViewResultadoTemp.setText(String.format("Resultado: %.2f °F", result));
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.radioFtoC) {
                    double result = (temp - 32) * 5 / 9;
                    textViewResultadoTemp.setText(String.format("Resultado: %.2f °C", result));
                }
            }
        });

        buttonLimpiarTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextTemp.setText("");
                textViewResultadoTemp.setText("");
            }
        });

        buttonRegresarTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
