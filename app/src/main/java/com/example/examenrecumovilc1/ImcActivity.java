package com.example.examenrecumovilc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class ImcActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        TextView textViewNombreImc = findViewById(R.id.textViewNombreImc);
        EditText editTextAltura = findViewById(R.id.editTextAltura);
        EditText editTextPeso = findViewById(R.id.editTextPeso);
        Button buttonCalcularImc = findViewById(R.id.buttonCalcularImc);
        TextView textViewResultadoImc = findViewById(R.id.textViewResultadoImc);
        Button buttonLimpiarImc = findViewById(R.id.buttonLimpiarImc);
        Button buttonRegresarImc = findViewById(R.id.buttonRegresarImc);

        String nombre = getIntent().getStringExtra("NOMBRE");
        textViewNombreImc.setText("Bienvenido, " + nombre);

        buttonCalcularImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double altura = Double.parseDouble(editTextAltura.getText().toString());
                double peso = Double.parseDouble(editTextPeso.getText().toString());
                double imc = peso / (altura * altura);
                textViewResultadoImc.setText(String.format("IMC: %.2f", imc));
            }
        });

        buttonLimpiarImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextAltura.setText("");
                editTextPeso.setText("");
                textViewResultadoImc.setText("");
            }
        });

        buttonRegresarImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
